$(document).ready(function (){
    $('input').keydown(function (){
        document.getElementById('error').innerHTML="";
    });
    $('.spinner-border-sm').hide();
});

$(".datepicker").datepicker({
    orientation: 'auto bottom',
    format: 'yyyy-mm-dd',
    autoclose: true
});

function logout(){
    window.location.href="../authentication/logout";
}
function adminHome(){
    window.location.href="../home/admin";
}

function createUser(){
    if(!validateCreate()){
        return false;
    }
    startLoadingAnimation();
    var user={
        firstName:$('#firstName').val(),
        lastName:$('#lastName').val(),
        email:$('#email').val(),
        password:$('#password').val(),
        birthday:$('#birthday').val(),
    }
    $.ajax({
        type:"POST",
        contentType: "application/json; charset=utf-8",
        data:JSON.stringify(user),
        url:"/user/create",
        success: function(result){
            if(result.status=="success"){
                stopLoadingAnimation();
                if(result.message.birthday==null){
                    document.getElementById('error').innerHTML= "Birthday can not be greater than today";
                }
                else{
                    document.getElementById("modalBody").innerHTML="User "+result.message.firstName+" was created successfully";
                    $("#userCreationConfirmation").modal('show');
                    $(".closeModal").click(function (){
                        window.location.href="/home/admin";
                    });
                }
            }
            else {
                document.getElementById('error').innerHTML= result.message;
                stopLoadingAnimation();
            }
        }
    });
}

function  validateCreate(){
    if($("#firstName").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'First name'";
        $("#firstName").focus();
        return false;
    }
    if($("#lastName").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Last name'";
        $("#lastName").focus();
        return false;
    }
    if($("#email").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Email'";
        $("#email").focus();
        return false;
    }
    if($("#password").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Password'";
        $("#password").focus();
        return false;
    }
    if($("#birthday").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Birthday'";
        $("#birthday").focus();
        return false;
    }

    var emailReg= /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!emailReg.test($("#email").val())){
        document.getElementById('error').innerHTML="Please insert email in correct format!";
        $("#email").focus();
        return false;
    }
    var passReg=/^[a-zA-Z0-9_.-].{8,}$/;
    if(!passReg.test($("#password").val())){
        document.getElementById('error').innerHTML="Password must be 8 letters long and must contain at least 1 number and 1 letter"
        $("#password").focus();
        return false;
    }
    return true;
}

function startLoadingAnimation(){
    $('.spinner-border-sm').show();
    $('.btnText').text('Creating the trip..');
    $('.btn-outline-success').attr("disabled", true);
}
function stopLoadingAnimation(){
    $('.spinner-border-sm').hide();
    $('.btnText').text('Create Trip');
    $('.btn-outline-success').attr("disabled", false);
}