
function logout(){
    window.location.href="../authentication/logout";
}
function adminHome(){
    window.location.href="../home/admin";
}

function approve(tripId){
    document.getElementById("error").innerHTML="";
    $.ajax({
        url: '/trip/approve?tripId='+tripId,
        type: 'PUT',
        success: function(result) {
            if(result.status=="success"){
                tripId="tr"+tripId;
                $("#"+tripId).remove();
            }
            else {
                document.getElementById("error").innerHTML=result.message;
            }
        }
    });
}

function disapprove(tripId){
    document.getElementById("error").innerHTML="";
    $.ajax({
        url: '/trip/disapprove?tripId='+tripId,
        type: 'PUT',
        success: function(result) {
            if(result.status=="success"){
                tripId="tr"+tripId;
                $("#"+tripId).remove();
            }
            else {
                document.getElementById("error").innerHTML=result.message;
            }
        }
    });
}