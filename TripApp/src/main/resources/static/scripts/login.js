$(document).ready(function (){
    $('input[type=email],input[type=password]').keydown(function (){
        document.getElementById('error').innerHTML="";
    });
    $('.spinner-border-sm').hide();
});

function login() {
    if(validateForm()){
        startLoadingAnimation();
        var user={
            email:$('#email').val(),
            password:$('#password').val()
        }
        $.ajax({
            type:"POST",
            contentType: "application/json; charset=utf-8",
            data:JSON.stringify(user),
            url:"/authentication/checkLogin",
            success: function(result){
               if(result.status=="success"){
                   window.location.href=result.message;
               }
               else {
                   document.getElementById('error').innerHTML= result.message;
                   stopLoadingAnimation();
               }
            }
        });
    }
}

function startLoadingAnimation(){
    $('.spinner-border-sm').show();
    $('.sr-only').text('Logging in...');
    $('.btn-primary').attr("disabled", true);
}
function stopLoadingAnimation(){
    $('.spinner-border-sm').hide();
    $('.sr-only').text('Log in');
    $('.btn-primary').attr("disabled", false);
}

function  validateForm(){
    var errorMsg=document.getElementById('error');

    if($("#email").val()==""){
        errorMsg.innerHTML="Please enter your email!";
        $("#email").focus();
        return false;
    }
    if($("#password").val()==""){
        errorMsg.innerHTML="Please enter your password!";
        $("#password").focus();
        return false;
    }

    var emailReg= /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!emailReg.test($("#email").val())){
        errorMsg.innerHTML="Please insert email in correct format!";
        $("#email").focus();
        return false;
    }
    var passReg=/^[a-zA-Z0-9_.-].{8,}$/;
    if(!passReg.test($("#password").val())){
        errorMsg.innerHTML="Password must be 8 letters long and must contain at least 1 number and 1 letter"
        $("#password").focus();
        return false;
    }
    return true;
}
