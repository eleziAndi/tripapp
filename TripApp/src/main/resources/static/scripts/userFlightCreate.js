$(document).ready(function (){
    $('input,textarea').keydown(function (){
        document.getElementById('error').innerHTML="";
    });
    $('.spinner-border-sm').hide();
});

$(".datepicker").datepicker({
    orientation: 'auto bottom',
    format: 'yyyy-mm-dd',
    autoclose: true
});

function logout(){
    window.location.href="../authentication/logout";
}
function userHome(){
    window.location.href="../home/user";
}

function saveFlight(tripId){
    if(!validateCreate()){
        return false;
    }
    startLoadingAnimation();
    var flight={
        from:$('#flightFrom').val(),
        to:$('#flightTo').val(),
        departure:$('#flightDeparture').val(),
        arrival:$('#flightArrival').val(),
        trip:{id:tripId}
    }
    $.ajax({
        type:"POST",
        contentType: "application/json; charset=utf-8",
        data:JSON.stringify(flight),
        url:"/flight/create",
        success: function(result){
            if(result.status=="success"){
                window.location.href=result.message;
            }
            else {
                document.getElementById('error').innerHTML= result.message;
                stopLoadingAnimation();
            }
        }
    });
}

function  validateCreate(){
    if($("#flightFrom").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'From'";
        $("#flightFrom").focus();
        return false;
    }
    if($("#flightTo").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'To'";
        $("#flightTo").focus();
        return false;
    }
    if($("#flightDeparture").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Departure date'";
        $("#flightDeparture").focus();
        return false;
    }
    if($("#flightArrival").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Arrival date'";
        $("#flightArrival").focus();
        return false;
    }
    return true;
}

function startLoadingAnimation(){
    $('.spinner-border-sm').show();
    $('.btnText').text('Adding flight..');
    $('.btn-outline-success').attr("disabled", true);
}
function stopLoadingAnimation(){
    $('.spinner-border-sm').hide();
    $('.btnText').text('Add flight');
    $('.btn-outline-success').attr("disabled", false);
}