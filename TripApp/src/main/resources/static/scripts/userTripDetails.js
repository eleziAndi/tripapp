$(document).ready(function (){
    $('.spinner-border-sm').hide();
});

function logout() {
    window.location.href = "../authentication/logout";
}

function userHome() {
    window.location.href = "../home/user";
}

function editTrip(tripId) {
    window.location.href = "../trip/edit?id=" + tripId;
}

function openDeleteModal(tripId) {
    document.getElementById("deleteTripId").innerHTML = tripId + "";
    $("#tripDeleteModal").modal('show');
}

function openFlightDeleteModal(flightId) {
    document.getElementById("deleteFlightId").innerHTML = flightId + "";
    $("#flightDeleteModal").modal('show');
}

function closeModal(modalId) {
    $("#" + modalId).modal('hide');
}

function deleteTrip() {
    var tripId = document.getElementById("deleteTripId").innerHTML;

    $.ajax({
        url: '/trip/delete?id='+tripId,
        type: 'DELETE',
        success: function(result) {
            window.location.href=result;
        }
    });
}

function deleteFlight(){
    var flightId = document.getElementById("deleteFlightId").innerHTML;
    $.ajax({
        url: '/flight/delete?id='+flightId,
        type: 'DELETE',
        success: function(result) {
            window.location.href=result;
        }
    });
}

function sendForApproval(tripId){
   startLoadingAnimation();
    $.ajax({
        url: '/trip/SendForApproval?tripId='+tripId,
        type: 'PUT',
        success: function(result) {
            if(result.status=="success"){
                stopLoadingAnimation();
                window.location.href="/trip/details?id="+tripId;
            }
        }
    });
}

function startLoadingAnimation(){
    $('.spinner-border-sm').show();
    $('.btnText').text('Sending request..');
    $('.btn-outline-success').attr("disabled", true);
}
function stopLoadingAnimation(){
    $('.spinner-border-sm').hide();
    $('.btnText').text('Send for approval');
    $('.btn-outline-success').attr("disabled", false);
}