$(document).ready(function (){
    $('input,textarea').keydown(function (){
        document.getElementById('error').innerHTML="";
    });
    $('.spinner-border-sm').hide();
});
$(".datepicker").datepicker({
    orientation: 'auto bottom',
    format: 'yyyy-mm-dd',
    autoclose: true
});
function logout(){
    window.location.href="../authentication/logout";
}
function userHome(){
    window.location.href="../home/user";
}

function createTrip(){
    if(!validateCreate()){
        return false;
    }
    startLoadingAnimation();
    var trip={
        description:$('#tripDescription').val(),
        from:$('#tripFrom').val(),
        to:$('#tripTo').val(),
        departure:$('#tripDeparture').val(),
        arrival:$('#tripArrival').val(),
        reason:{id:$('#tripReason').val()}
    }
    $.ajax({
        type:"POST",
        contentType: "application/json; charset=utf-8",
        data:JSON.stringify(trip),
        url:"/trip/create",
        success: function(result){
            if(result.status=="success"){
                if(result.message.departure==null && result.arrival==null){
                    document.getElementById('error').innerHTML= "Arrival should be bigger than Departure date!";
                    stopLoadingAnimation();
                }
               else if(result.message.departure==null){
                    document.getElementById('error').innerHTML= "Departure should be bigger than today's date!";
                    stopLoadingAnimation();
                }
               else if(result.message.arrival==null){
                    document.getElementById('error').innerHTML= "Arrival should be bigger than today's date!";
                    stopLoadingAnimation();
                }

               else{
                    window.location.href="/trip/details?id="+result.message.id;
                }

            }
            else {
                document.getElementById('error').innerHTML= result.message;
                stopLoadingAnimation();
            }
        }
    });
}

function  validateCreate(){
    if($("#tripDescription").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Description'";
        $("#tripDescription").focus();
        return false;
    }
    if($("#tripReason").val()=="-1"){
        document.getElementById('error').innerHTML="Please select one of the reasons!";
        $("#tripReason").focus();
        return false;
    }
    if($("#tripFrom").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'From'";
        $("#tripFrom").focus();
        return false;
    }
    if($("#tripTo").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'To'";
        $("#tripTo").focus();
        return false;
    }
    if($("#tripDeparture").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Departure date'";
        $("#tripDeparture").focus();
        return false;
    }
    if($("#tripArrival").val()==""){
        document.getElementById('error').innerHTML="Please fill in the field 'Arrival date'";
        $("#tripArrival").focus();
        return false;
    }
    return true;
}

function startLoadingAnimation(){
    $('.spinner-border-sm').show();
    $('.btnText').text('Creating the trip..');
    $('.btn-outline-success').attr("disabled", true);
}
function stopLoadingAnimation(){
    $('.spinner-border-sm').hide();
    $('.btnText').text('Create Trip');
    $('.btn-outline-success').attr("disabled", false);
}