package com.lhind.TripApp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedNativeQuery(name ="Role.findAllRoles",query = "SELECT * from role",resultClass = Trip.class)
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="description")
    private String description;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "role")
    @JsonManagedReference(value = "userRole")
    @OrderBy("id")
    private List<User> users;

    public Role() {
    }

    public Role(int id, String description, List<User> users) {
        this.id = id;
        this.description = description;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
