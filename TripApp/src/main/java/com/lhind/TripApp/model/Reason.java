package com.lhind.TripApp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedNativeQuery(name ="Reason.findAllReasons",query = "SELECT * from reason ",resultClass = Reason.class)
public class Reason {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "description")
    private String description;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "reason")
    @JsonManagedReference(value = "tripReason")
    @OrderBy("id")
    private List<Trip> trips;

    public Reason() {
    }

    public Reason(int id, String description, List<Trip> trips) {
        this.id = id;
        this.description = description;
        this.trips = trips;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}
