package com.lhind.TripApp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedNativeQuery(name ="Trip.findAllTrips",query = "SELECT * from trip",resultClass = Trip.class)
@NamedNativeQuery(name ="Trip.findByStatus",query = "SELECT * from trip WHERE  status=?",resultClass = Trip.class)
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="description")
    private String description;

    @Column(name="start_location")
    private String from;

    @Column(name="destination")
    private String to;

    @Column(name="departure")
    @Temporal(TemporalType.DATE)
    private Date departure;

    @Column(name="arrival")
    @Temporal(TemporalType.DATE)
    private Date arrival;

    @Column(name="status")
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="reason_id")
    @JsonBackReference(value = "tripReason")
    private Reason reason;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    @JsonBackReference(value = "userTrip")
    private User user;

    @OneToMany(mappedBy = "trip",cascade = CascadeType.REMOVE)
    @JsonManagedReference(value = "tripFlight")
    @OrderBy("id")
    private List<Flight> flights;

    public Trip() {
    }

    public Trip(int id, String description, String from, String to, Date departure, Date arrival, String status, Reason reason, User user, List<Flight> flights) {
        this.id = id;
        this.description = description;
        this.from = from;
        this.to = to;
        this.departure = departure;
        this.arrival = arrival;
        this.status = status;
        this.reason = reason;
        this.user = user;
        this.flights = flights;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}
