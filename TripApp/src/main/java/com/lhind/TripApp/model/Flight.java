package com.lhind.TripApp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedNativeQuery(name ="Flight.findAllFlights",query = "SELECT * from flight ",resultClass = Flight.class)
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  int id;

    @Column(name="start_location")
    private String from;

    @Column(name="destination")
    private String to;

    @Column(name="departure")
    @Temporal(TemporalType.DATE)
    private Date departure;

    @Column(name="arrival")
    @Temporal(TemporalType.DATE)
    private Date arrival;

    @ManyToOne
    @JoinColumn(name="trip_id")
    @JsonBackReference(value = "tripFlight")
    private Trip trip;

    public Flight() {
    }

    public Flight(int id, String from, String to, Date departure, Date arrival, Trip trip) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.departure = departure;
        this.arrival = arrival;
        this.trip = trip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }


}
