package com.lhind.TripApp.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lhind.TripApp.model.Role;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.repository.interfaces.IUserRepository;
import com.lhind.TripApp.service.interfaces.IUserService;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Locale;

@Service
public class UserServiceImpl implements IUserService {
    final
    IUserRepository userRepository;

    public UserServiceImpl(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String addUser(User user,Role role,String passwordKey) throws JsonProcessingException {
        ObjectMapper mapper=new ObjectMapper();
        long millis=System.currentTimeMillis();
        Date today=new java.sql.Date(millis);
        user.setEmail(user.getEmail().toLowerCase());
        if(userRepository.getUser(user.getEmail())!=null){
            return "";
        }
        if(user.getBirthday().compareTo(today)>0){
            user.setBirthday(null);
            return mapper.writeValueAsString(user);
        }
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(passwordKey);
        String encryptedPassword= encryptor.encrypt(user.getPassword());
        user.setPassword(encryptedPassword);
        user.setRole(role);
        return mapper.writeValueAsString(userRepository.addUser(user));
    }

    @Override
    public User getUser(int userId) {
        return  userRepository.getUser(userId);
    }

    @Override
    public User getUser(String email, String password,String passwordKey) {
        try{
            User user=userRepository.getUser(email);
            if(user==null){
                return  null;
            }
            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword(passwordKey);
            String decryptedPassword=encryptor.decrypt(user.getPassword());
            if(password.compareTo(decryptedPassword)!=0){
                return null;
            }
            return user;
        }
        catch (Exception e){
            return null;
        }

    }

    @Override
    public User updateUser(User updatedUser) {
        return userRepository.updateUser(updatedUser);
    }

    @Override
    public User deleteUser(int userID) {
        return userRepository.deleteUser(userID);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
}
