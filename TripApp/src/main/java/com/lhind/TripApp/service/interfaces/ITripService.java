package com.lhind.TripApp.service.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lhind.TripApp.model.Reason;
import com.lhind.TripApp.model.Trip;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ITripService {
    public String addTrip(Trip trip, HttpSession session) throws JsonProcessingException;
    public Trip getTrip(int tripId);
    public Trip updateTrip(Trip tripToChange, Trip updatedTrip);
    public Trip updateTripStatus(Trip trip,String newStatus);
    public Trip deleteTrip(int tripID);
    public List<Trip> getAllTrips();
    public List<Trip> getTripApplications();
}
