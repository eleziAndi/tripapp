package com.lhind.TripApp.service.interfaces;

import com.lhind.TripApp.model.Reason;

import java.util.List;

public interface IReasonService {
    public Reason addReason(Reason reason);
    public Reason getReason(int reasonId);
    public Reason updateReason(Reason updatedReason);
    public Reason deleteReason(int reasonID);
    public List<Reason> getAllReasons();
}
