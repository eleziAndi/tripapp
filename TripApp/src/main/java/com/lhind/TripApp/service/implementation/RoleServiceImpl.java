package com.lhind.TripApp.service.implementation;

import com.lhind.TripApp.model.Role;
import com.lhind.TripApp.repository.interfaces.IRoleRepository;
import com.lhind.TripApp.service.interfaces.IRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {
    final
    IRoleRepository roleRepository;

    public RoleServiceImpl(IRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role addRole(Role role) {
        return roleRepository.addRole(role);
    }

    @Override
    public Role getRole(int roleId) {
        return  roleRepository.getRole(roleId);
    }

    @Override
    public Role updateRole(Role updatedRole) {
        return roleRepository.updateRole(updatedRole);
    }

    @Override
    public Role deleteRole(int roleID) {
        return roleRepository.deleteRole(roleID);
    }

    @Override
    public List<Role> getAllRoles() {
        return roleRepository.getAllRoles();
    }
}
