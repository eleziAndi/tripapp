package com.lhind.TripApp.service.interfaces;

import com.lhind.TripApp.model.Flight;

import java.util.List;

public interface IFlightService {
    public Flight addFlight(Flight flight);
    public Flight getFlight(int flightId);
    public Flight updateFlight(Flight updatedFlight);
    public Flight deleteFlight(int flightID);
    public List<Flight> getAllFlights();
}
