package com.lhind.TripApp.service.implementation;

import com.lhind.TripApp.model.Flight;
import com.lhind.TripApp.repository.interfaces.IFlightRepository;
import com.lhind.TripApp.service.interfaces.IFlightService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements IFlightService {
    final
    IFlightRepository flightRepository;

    public FlightServiceImpl(IFlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    @Override
    public Flight addFlight(Flight flight) {
        return flightRepository.addFlight(flight);
    }

    @Override
    public Flight getFlight(int flightId) {
        return  flightRepository.getFlight(flightId);
    }

    @Override
    public Flight updateFlight(Flight updatedFlight) {
        return flightRepository.updateFlight(updatedFlight);
    }

    @Override
    public Flight deleteFlight(int flightID) {
        return flightRepository.deleteFlight(flightID);
    }

    @Override
    public List<Flight> getAllFlights() {
        return flightRepository.getAllFlights();
    }
}
