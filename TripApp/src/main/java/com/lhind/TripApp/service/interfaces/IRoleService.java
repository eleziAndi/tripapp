package com.lhind.TripApp.service.interfaces;

import com.lhind.TripApp.model.Role;

import java.util.List;

public interface IRoleService {
    public Role addRole(Role role);
    public Role getRole(int roleId);
    public Role updateRole(Role updatedRole);
    public Role deleteRole(int roleID);
    public List<Role> getAllRoles();
}
