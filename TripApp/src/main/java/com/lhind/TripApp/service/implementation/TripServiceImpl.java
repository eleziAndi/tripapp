package com.lhind.TripApp.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.repository.interfaces.ITripRepository;
import com.lhind.TripApp.service.interfaces.ITripService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TripServiceImpl implements ITripService {
    final
    ITripRepository tripRepository;

    public TripServiceImpl(ITripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    @Override
    public String addTrip(Trip trip, HttpSession session) throws JsonProcessingException {
        ObjectMapper mapper=new ObjectMapper();
        long millis=System.currentTimeMillis();
        Date today=new java.sql.Date(millis);
        if(trip.getDeparture().compareTo(today)<0){
            trip.setDeparture(null);
            return mapper.writeValueAsString(trip);
        }
        else if(trip.getArrival().compareTo(today)<0){
            trip.setArrival(null);
            return mapper.writeValueAsString(trip);
        }
        else if(trip.getDeparture().compareTo(trip.getArrival())>=0){
            trip.setDeparture(null);
            trip.setArrival(null);
            return mapper.writeValueAsString(trip);
        }
        trip.setUser((User)session.getAttribute("loggedUser"));
        trip.setStatus("CREATED");
        return mapper.writeValueAsString(tripRepository.addTrip(trip));
    }

    @Override
    public Trip getTrip(int tripId) {
        return  tripRepository.getTrip(tripId);
    }

    @Override
    public Trip updateTrip(Trip tripToChange, Trip updatedTrip) {
        tripToChange.setDescription(updatedTrip.getDescription());
        tripToChange.setReason(updatedTrip.getReason());
        tripToChange.setFrom(updatedTrip.getFrom());
        tripToChange.setTo(updatedTrip.getTo());
        tripToChange.setDeparture(updatedTrip.getDeparture());
        tripToChange.setArrival(updatedTrip.getArrival());
        return tripRepository.updateTrip(tripToChange);
    }

    @Override
    public Trip updateTripStatus(Trip trip,String newStatus) {
       trip.setStatus(newStatus);
       return tripRepository.updateTrip(trip);
    }

    @Override
    public Trip deleteTrip(int tripID) {
        return tripRepository.deleteTrip(tripID);
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripRepository.getAllTrips();
    }

    @Override
    public List<Trip> getTripApplications() {
        return tripRepository.getTripsByStatus("WAITING FOR APPROVAL");
    }
}
