package com.lhind.TripApp.service.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lhind.TripApp.model.Role;
import com.lhind.TripApp.model.User;

import java.util.List;

public interface IUserService {
    public String addUser(User user, Role role,String passwordKey) throws JsonProcessingException;
    public User getUser(int userId);
    public User getUser(String email,String password,String passwordKey);
    public User updateUser(User updatedUser);
    public User deleteUser(int userID);
    public List<User> getAllUsers();
}
