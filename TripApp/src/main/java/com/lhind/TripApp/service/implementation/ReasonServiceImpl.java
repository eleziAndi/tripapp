package com.lhind.TripApp.service.implementation;

import com.lhind.TripApp.model.Reason;
import com.lhind.TripApp.repository.interfaces.IReasonRepository;
import com.lhind.TripApp.service.interfaces.IReasonService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReasonServiceImpl implements IReasonService {
    final
    IReasonRepository reasonRepository;

    public ReasonServiceImpl(IReasonRepository reasonRepository) {
        this.reasonRepository = reasonRepository;
    }

    @Override
    public Reason addReason(Reason reason) {
        return reasonRepository.addReason(reason);
    }

    @Override
    public Reason getReason(int reasonId) {
        return reasonRepository.getReason(reasonId);
    }

    @Override
    public Reason updateReason(Reason updatedReason) {
        return reasonRepository.updateReason(updatedReason);
    }

    @Override
    public Reason deleteReason(int reasonID) {
        return reasonRepository.deleteReason(reasonID);
    }

    @Override
    public List<Reason> getAllReasons() {
        return reasonRepository.getAllReasons();
    }
}
