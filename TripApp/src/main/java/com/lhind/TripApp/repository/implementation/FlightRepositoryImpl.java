package com.lhind.TripApp.repository.implementation;

import com.lhind.TripApp.model.Flight;
import com.lhind.TripApp.repository.interfaces.IFlightRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class FlightRepositoryImpl implements IFlightRepository {
    private final EntityManager entityManager;

    public FlightRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Flight addFlight(Flight flight) {
       entityManager.persist(flight);
       return  flight;
    }

    @Override
    @Transactional
    public Flight getFlight(int flightId) {
        return  entityManager.find(Flight.class,new Integer(flightId));
    }

    @Override
    @Transactional
    public Flight updateFlight(Flight updatedFlight) {
        entityManager.merge(updatedFlight);
        return  updatedFlight;
    }

    @Override
    @Transactional
    public Flight deleteFlight(int flightId) {
        Flight flightToBeDeleted=getFlight(flightId);
        entityManager.remove(flightToBeDeleted);
        return flightToBeDeleted;
    }

    @Override
    @Transactional
    public List<Flight> getAllFlights() {
        Query q = entityManager.createNamedQuery("Flight.findAllFlights");
        List<Flight> flights = q.getResultList();
        return flights;
    }
}
