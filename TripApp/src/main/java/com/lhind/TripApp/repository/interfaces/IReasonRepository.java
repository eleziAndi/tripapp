package com.lhind.TripApp.repository.interfaces;

import com.lhind.TripApp.model.Reason;

import java.util.List;

public interface IReasonRepository {
    public Reason addReason(Reason reason);
    public Reason getReason(int reasonId);
    public Reason updateReason(Reason updatedReason);
    public Reason deleteReason(int reasonID);
    public List<Reason> getAllReasons();
}
