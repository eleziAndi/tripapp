package com.lhind.TripApp.repository.interfaces;

import com.lhind.TripApp.model.Trip;

import java.util.List;

public interface ITripRepository {
    public Trip addTrip(Trip trip);
    public Trip getTrip(int tripId);
    public Trip updateTrip(Trip updatedTrip);
    public Trip deleteTrip(int tripID);
    public List<Trip> getAllTrips();
    public List<Trip> getTripsByStatus(String status);
}
