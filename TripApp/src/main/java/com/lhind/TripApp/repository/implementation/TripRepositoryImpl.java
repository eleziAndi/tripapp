package com.lhind.TripApp.repository.implementation;

import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.repository.interfaces.ITripRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TripRepositoryImpl implements ITripRepository {
    private final EntityManager entityManager;

    public TripRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Trip addTrip(Trip trip) {
        entityManager.persist(trip);
        return  trip;
    }

    @Override
    @Transactional
    public Trip getTrip(int tripId) {
        return  entityManager.find(Trip.class,new Integer(tripId));
    }

    @Override
    @Transactional
    public Trip updateTrip(Trip updatedTrip) {
        entityManager.merge(updatedTrip);
        return  updatedTrip;
    }

    @Override
    @Transactional
    public Trip deleteTrip(int tripId) {
        Trip tripToBeDeleted=getTrip(tripId);
        entityManager.remove(tripToBeDeleted);
        return tripToBeDeleted;
    }

    @Override
    @Transactional
    public List<Trip> getAllTrips() {
        Query q = entityManager.createNamedQuery("Trip.findAllTrips");
        List<Trip> trips = q.getResultList();
        return trips;
    }

    @Override
    public List<Trip> getTripsByStatus(String status) {
        Query q = entityManager.createNamedQuery("Trip.findByStatus");
        q.setParameter(1,status);
        List<Trip> trips=q.getResultList();
        return trips;
    }
}
