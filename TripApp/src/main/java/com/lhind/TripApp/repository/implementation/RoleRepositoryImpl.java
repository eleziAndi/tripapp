package com.lhind.TripApp.repository.implementation;

import com.lhind.TripApp.model.Role;
import com.lhind.TripApp.repository.interfaces.IRoleRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class RoleRepositoryImpl implements IRoleRepository {
    private final EntityManager entityManager;

    public RoleRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Role addRole(Role role) {
        entityManager.persist(role);
        return  role;
    }

    @Override
    @Transactional
    public Role getRole(int roleId) {
        return  entityManager.find(Role.class,new Integer(roleId));
    }

    @Override
    @Transactional
    public Role updateRole(Role updatedRole) {
        entityManager.merge(updatedRole);
        return  updatedRole;
    }

    @Override
    @Transactional
    public Role deleteRole(int roleId) {
        Role roleToBeDeleted=getRole(roleId);
        entityManager.remove(roleToBeDeleted);
        return roleToBeDeleted;
    }

    @Override
    @Transactional
    public List<Role> getAllRoles() {
        Query q = entityManager.createNamedQuery("Role.findAllRoles");
        List<Role> roles = q.getResultList();
        return roles;
    }
}
