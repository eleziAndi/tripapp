package com.lhind.TripApp.repository.interfaces;

import com.lhind.TripApp.model.User;

import java.util.List;

public interface IUserRepository {
    public User addUser(User user);
    public User getUser(int userId);
    public User getUser(String email,String password);
    public User getUser(String email);
    public User updateUser(User updatedUser);
    public User deleteUser(int userID);
    public List<User> getAllUsers();
}
