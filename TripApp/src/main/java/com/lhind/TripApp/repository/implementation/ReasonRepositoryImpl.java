package com.lhind.TripApp.repository.implementation;

import com.lhind.TripApp.model.Reason;
import com.lhind.TripApp.repository.interfaces.IReasonRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ReasonRepositoryImpl implements IReasonRepository {
    private final EntityManager entityManager;

    public ReasonRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public Reason addReason(Reason reason) {
        entityManager.persist(reason);
        return  reason;
    }

    @Override
    @Transactional
    public Reason getReason(int reasonId) {
        return  entityManager.find(Reason.class,new Integer(reasonId));
    }

    @Override
    @Transactional
    public Reason updateReason(Reason updatedReason) {
        entityManager.merge(updatedReason);
        return  updatedReason;
    }

    @Override
    @Transactional
    public Reason deleteReason(int reasonId) {
        Reason reasonToBeDeleted=getReason(reasonId);
        entityManager.remove(reasonToBeDeleted);
        return reasonToBeDeleted;
    }

    @Override
    @Transactional
    public List<Reason> getAllReasons() {
        Query q = entityManager.createNamedQuery("Reason.findAllReasons");
        List<Reason> reasons = q.getResultList();
        return reasons;
    }
}
