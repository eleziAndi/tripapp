package com.lhind.TripApp.repository.implementation;

import com.lhind.TripApp.model.User;
import com.lhind.TripApp.repository.interfaces.IUserRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UserRepositoryImpl implements IUserRepository {
    private final EntityManager entityManager;

    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public User addUser(User user) {
        entityManager.persist(user);
        return  user;
    }

    @Override
    @Transactional
    public User getUser(int userId) {
        return  entityManager.find(User.class,new Integer(userId));
    }

    @Override
    public User getUser(String email, String password) {
        Query q = entityManager.createNamedQuery("User.findLoggedUser");
        q.setParameter(1,email);
        q.setParameter(2,password);
        List<User> users=q.getResultList();
        if(users.size()==0){
            return null;
        }
        else{
            return users.get(0);
        }
    }

    @Override
    public User getUser(String email) {
        Query q = entityManager.createNamedQuery("User.findByEmail");
        q.setParameter(1,email);
        List<User> users=q.getResultList();
        if(users.size()==0){
            return null;
        }
        else{
            return users.get(0);
        }
    }

    @Override
    @Transactional
    public User updateUser(User updatedUser) {
        entityManager.merge(updatedUser);
        return  updatedUser;
    }

    @Override
    @Transactional
    public User deleteUser(int userId) {
        User userToBeDeleted=getUser(userId);
        entityManager.remove(userToBeDeleted);
        return userToBeDeleted;
    }

    @Override
    @Transactional
    public List<User> getAllUsers() {
        Query q = entityManager.createNamedQuery("User.findAllUsers");
        List<User> users = q.getResultList();
        return users;
    }
}
