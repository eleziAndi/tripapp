package com.lhind.TripApp.repository.interfaces;

import com.lhind.TripApp.model.Role;

import java.util.List;

public interface IRoleRepository {
    public Role addRole(Role role);
    public Role getRole(int roleId);
    public Role updateRole(Role updatedRole);
    public Role deleteRole(int roleID);
    public List<Role> getAllRoles();
}
