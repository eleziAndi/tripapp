package com.lhind.TripApp.controller;

import com.lhind.TripApp.model.Flight;
import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.service.interfaces.IFlightService;
import com.lhind.TripApp.service.interfaces.ITripService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(path = "/flight")
public class FlightController {
    final
    IFlightService flightService;
    final
    ITripService tripService;
    public final static Logger logger = Logger.getLogger(AuthenticationController.class);

    public FlightController(IFlightService flightService,ITripService tripService) {
        this.flightService = flightService;
        this.tripService = tripService;
        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");
    }


    @DeleteMapping(path = "/delete")
    @ResponseBody
    public String deleteFlight(@RequestParam(name="id") int flightId, HttpSession session){

        try {
            if(!checkUserAuthentication(session)){
                return "/authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            Flight flight=flightService.getFlight(flightId);
            if(flight.getTrip().getUser().getId()!=loggedUser.getId()){
                return "/trip/details?id="+flight.getTrip().getId();
            }
            flightService.deleteFlight(flightId);
            return "/trip/details?id="+flight.getTrip().getId();
        }
        catch (Exception e){
            logger.error("An error occurred while trying to delete a flight: "+e);
            return "/authentication/login";
        }


    }

    @GetMapping(path="/create")
    public String createFlight(@RequestParam("tripId") int tripId, Model model,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "redirect:../authentication/login";
            }
            Trip trip=tripService.getTrip(tripId);
            User loggedUser=(User)session.getAttribute("loggedUser");
            if(trip.getUser().getId()!=loggedUser.getId()){
                return "redirect:../trip/details?id="+tripId;
            }
            model.addAttribute("loggedUser",loggedUser);
            model.addAttribute("trip",trip);
            return "Flight/create";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to create a flight: "+e);
            return "redirect:../trip/details?id="+tripId;
        }

    }

    @PostMapping(path="/create",produces = "application/json")
    @ResponseBody
    public String saveFlight(@RequestBody Flight flight,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to create a flight!\"}";
            }
            else {
                Trip trip=tripService.getTrip(flight.getTrip().getId());
                User loggedUser=(User)session.getAttribute("loggedUser");
                if(trip.getUser().getId()!=loggedUser.getId()){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to create a flight for this trip!\"}";
                }
                Flight addedFlight=flightService.addFlight(flight);
                return "{\"status\":\"success\",\"message\":\" /trip/details?id="+addedFlight.getTrip().getId()+"\"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to save a flight: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }


    }

    private boolean checkUserAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=1){
            return false;
        }
        return true;
    }
}
