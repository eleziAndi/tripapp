package com.lhind.TripApp.controller;

import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.service.interfaces.ITripService;
import com.lhind.TripApp.service.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(path = "/home")
public class HomeController {
    final
    ITripService tripService;
    final
    IUserService userService;
    public final static Logger logger = Logger.getLogger(AuthenticationController.class);

    public HomeController(ITripService tripService, IUserService userService) {
        this.tripService = tripService;
        this.userService = userService;
        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");
    }

    @GetMapping(path="/user")
    public String userHome(Model model, HttpSession session){
        try{
            if(!checkUserAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            loggedUser=userService.getUser(loggedUser.getId());
            model.addAttribute("tripList",loggedUser.getTrips());
            model.addAttribute("loggedUser",loggedUser);
            return "Home/userHome";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to access user home: "+e);
            return "redirect:../authentication/login";
        }
    }

    @GetMapping(path="/admin")
    public String adminHome(Model model,HttpSession session){
        try {
            if(!checkAdminAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            model.addAttribute("loggedUser",loggedUser);
            return "Home/adminHome";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to access admin home: "+e);
            return "redirect:../authentication/login";
        }
    }

    private boolean checkUserAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=1){
            return false;
        }
        return true;
    }


    private boolean checkAdminAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=2){
            return false;
        }
        return true;
    }
}
