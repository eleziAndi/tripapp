package com.lhind.TripApp.controller;

import com.lhind.TripApp.model.User;
import com.lhind.TripApp.service.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONObject;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

@Controller
@RequestMapping(path = "/authentication")
public class AuthenticationController {
    final
    IUserService userService;
    final
    Environment environment;
    public final static Logger logger = Logger.getLogger(AuthenticationController.class);

    public AuthenticationController(IUserService userService,Environment environment) {
        this.userService = userService;
        this.environment = environment;
        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");
    }

    @GetMapping(path = "/login")
    public String login(Model model){
        return "Authentication/login";
    }

    @PostMapping(path = "/checkLogin",produces = "application/json")
    @ResponseBody
    public String checkLogin(@RequestBody  User user, HttpSession session){
        try{
            User loggedUser=userService.getUser(user.getEmail().toLowerCase(), user.getPassword(),environment.getProperty("app.passwordKey"));
            if(loggedUser!=null){
                session.setAttribute("loggedUser",loggedUser);
                if(loggedUser.getRole().getId()==1){
                  return "{\"status\":\"success\",\"message\":\"../home/user\"}";
                }
                else if(loggedUser.getRole().getId()==2){
                    return "{\"status\":\"success\",\"message\":\"../home/admin\"}";
                }
                return "{\"status\":\"failure\",\"message\":\"\"}";
            }
            else{
                return "{\"status\":\"failure\",\"message\":\"Email or password is not correct\"}";
            }

        }
        catch(Exception e){
            logger.error("An error occurred while trying to login: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpSession session){
        session.setAttribute("loggedUser",null);
        return "redirect:../authentication/login";
    }
}
