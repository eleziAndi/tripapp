package com.lhind.TripApp.controller;

import com.lhind.TripApp.model.Reason;
import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.service.interfaces.IReasonService;
import com.lhind.TripApp.service.interfaces.ITripService;
import com.lhind.TripApp.service.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(path = "/trip")
public class TripController {
    final
    ITripService tripService;
    final
    IReasonService reasonService;
    final
    IUserService userService;
    public final static Logger logger = Logger.getLogger(AuthenticationController.class);

    public TripController(ITripService tripService, IReasonService reasonService, IUserService userService) {
        this.tripService = tripService;
        this.reasonService = reasonService;
        this.userService = userService;
        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");
    }

    @GetMapping(path = "/details")
    public String details(Model model, HttpSession session, @RequestParam(name="id") int tripId){
        try {
            if(!checkUserAuthentication(session)){
                return "redirect:../authentication/login";
            }
            Trip trip=tripService.getTrip(tripId);
            User loggedUser=(User)session.getAttribute("loggedUser");
            if(loggedUser.getId()!=trip.getUser().getId()){
                return "redirect:../home/user";
            }
            model.addAttribute("trip",trip);
            model.addAttribute("loggedUser",loggedUser);
            return "Trip/details";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to access trip details: "+e);
            return "redirect:../home/user";
        }

    }

    @GetMapping(path = "/edit")
    public String edit(Model model, HttpSession session, @RequestParam(name="id") int tripId){
        try {
            if(!checkUserAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            Trip trip=tripService.getTrip(tripId);
            if(loggedUser.getId()!=trip.getUser().getId() || trip.getStatus().compareTo("CREATED")!=0){
                return "redirect:../home/user";
            }
            model.addAttribute("trip",trip);
            model.addAttribute("loggedUser",loggedUser);
            model.addAttribute("reasonList",reasonService.getAllReasons());
            return "Trip/edit";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to access trip edit: "+e);
            return "redirect:../home/user";
        }

    }

    @PostMapping(path="/edit",produces = "application/json")
    @ResponseBody
    public String editTrip(@RequestBody Trip trip,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
            }
            else {
                Trip oldTrip=tripService.getTrip(trip.getId());
                User loggedUser=(User)session.getAttribute("loggedUser");
                if(oldTrip.getUser().getId()!=loggedUser.getId()){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
                }
                Trip newTrip=tripService.updateTrip(oldTrip,trip);
                return "{\"status\":\"success\",\"message\":\" /trip/details?id="+newTrip.getId()+"\"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to edit a trip: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }

    @GetMapping(path="/create")
    public String createTrip(Model model,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            model.addAttribute("loggedUser",loggedUser);
            model.addAttribute("reasonList",reasonService.getAllReasons());
            return "Trip/create";
        }
        catch (Exception e){
            logger.error("An error occurred while trying access create trip: "+e);
            return "redirect:../authentication/login";
        }
    }

    @PostMapping(path="/create",produces = "application/json")
    @ResponseBody
    public String saveTrip(@RequestBody Trip trip,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to create a trip!\"}";
            }
            else {
                String addedTrip=tripService.addTrip(trip,session);
                return "{\"status\":\"success\",\"message\":"+addedTrip+"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to save a trip: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }


    @DeleteMapping(path = "/delete")
    @ResponseBody
    public String deleteTrip(@RequestParam(name="id") int tripId,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "/authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            Trip trip=tripService.getTrip(tripId);
            if(trip.getUser().getId()!=loggedUser.getId()){
                return "/trip/details?id="+tripId;
            }
            tripService.deleteTrip(tripId);
            return "/home/user";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to delete a trip: "+e);
            return "/home/user";
        }


    }


    @PutMapping(path="/SendForApproval",produces = "application/json")
    @ResponseBody
    public String sendForApproval(@RequestParam("tripId") int tripId,HttpSession session){
        try {
            if(!checkUserAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
            }
            else {
                Trip trip=tripService.getTrip(tripId);
                User loggedUser=(User)session.getAttribute("loggedUser");
                if(trip.getUser().getId()!=loggedUser.getId()){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
                }
                if(trip.getStatus().compareTo("CREATED")!=0){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
                }
                trip=tripService.updateTripStatus(trip,"WAITING FOR APPROVAL");
                return "{\"status\":\"success\",\"newStatus\":\""+trip.getStatus()+"\"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to send for approval: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }

    @GetMapping(path="/manage")
    public String manageTripStatus(Model model, HttpSession session){
        try {
            if(!checkAdminAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            model.addAttribute("tripList",tripService.getTripApplications());
            model.addAttribute("loggedUser",loggedUser);
            return "Trip/manageApplications";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to mage trip status: "+e);
            return "redirect:../authentication/login";
        }

    }

    @PutMapping(path="/approve",produces = "application/json")
    @ResponseBody
    public String approveTripApplication(@RequestParam("tripId") int tripId,HttpSession session){

        try {
            if(!checkAdminAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
            }
            else {
                Trip trip=tripService.getTrip(tripId);
                User loggedUser=(User)session.getAttribute("loggedUser");
                if(trip.getStatus().compareTo("WAITING FOR APPROVAL")!=0){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
                }
                trip=tripService.updateTripStatus(trip,"APPROVED");
                return "{\"status\":\"success\",\"newStatus\":\""+trip.getStatus()+"\"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to approve a trip: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }

    @PutMapping(path="/disapprove",produces = "application/json")
    @ResponseBody
    public String disapproveTripApplication(@RequestParam("tripId") int tripId,HttpSession session){
        try {
            if(!checkAdminAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
            }
            else {
                Trip trip=tripService.getTrip(tripId);
                if(trip.getStatus().compareTo("WAITING FOR APPROVAL")!=0){
                    return "{\"status\":\"failure\",\"message\":\"You don't have permission to edit this trip!\"}";
                }
                trip=tripService.updateTripStatus(trip,"DISAPPROVED");
                return "{\"status\":\"success\",\"newStatus\":\""+trip.getStatus()+"\"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to disapprove a trip: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }

    private boolean checkUserAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=1){
            return false;
        }
        return true;
    }

    private boolean checkAdminAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=2){
            return false;
        }
        return true;
    }
}
