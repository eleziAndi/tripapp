package com.lhind.TripApp.controller;

import com.lhind.TripApp.model.Trip;
import com.lhind.TripApp.model.User;
import com.lhind.TripApp.service.interfaces.IRoleService;
import com.lhind.TripApp.service.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(path = "/user")

public class UserController {
    final
    Environment environment;
    final
    IUserService userService;
    final
    IRoleService roleService;
    public final static Logger logger = Logger.getLogger(AuthenticationController.class);

    public UserController(IUserService userService, IRoleService roleService, Environment environment) {
        this.userService = userService;
        this.roleService = roleService;
        this.environment = environment;
        PropertyConfigurator.configure(getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "log4j.properties");

    }

    @GetMapping(path="/create")
    public String createUser(Model model, HttpSession session){
        try {
            if(!checkAdminAuthentication(session)){
                return "redirect:../authentication/login";
            }
            User loggedUser=(User)session.getAttribute("loggedUser");
            model.addAttribute("loggedUser",loggedUser);
            return "User/create";
        }
        catch (Exception e){
            logger.error("An error occurred while trying to create user: "+e);
            return "redirect:../authentication/login";
        }

    }

    @PostMapping(path="/create",produces = "application/json")
    @ResponseBody
    public String saveUser(@RequestBody User user, HttpSession session){
        try {
            if(!checkAdminAuthentication(session)){
                return "{\"status\":\"failure\",\"message\":\"You don't have permission to create a user!\"}";
            }
            else {
                String addedUser=userService.addUser(user,roleService.getRole(1),environment.getProperty("app.passwordKey"));
                if(addedUser.compareTo("")==0){
                    return "{\"status\":\"failure\",\"message\":\"Email you entered already exist!\"}";
                }
                return "{\"status\":\"success\",\"message\":"+addedUser+"}";
            }
        }
        catch (Exception e){
            logger.error("An error occurred while trying to save a user: "+e);
            return "{\"status\":\"failure\",\"message\":\"Something wrong happened, please try again later!\"}";
        }

    }

    private boolean checkAdminAuthentication(HttpSession session){
        if(session.getAttribute("loggedUser")==null){
            return false;
        }
        User loggedUser=(User)session.getAttribute("loggedUser");
        if(loggedUser.getRole().getId()!=2){
            return false;
        }
        return true;
    }
}

